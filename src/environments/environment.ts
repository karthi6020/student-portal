// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  userInfo: {
    name: 'kkk',
    pwd: '123'
  },
  headers: {
    param: 'Authorization',
    value: 'Basic YXV0aGVudGljYTpAdXRoM250MWNA',
    username: 'authentica',
    password: '@uth3nt1c@'
  },
  api: {
    student: {
      view: {
        method: 'get',
        url: 'http://interviewapi20170221095727.azurewebsites.net/api/Student/All'
      },
      enrollment: {
        method: 'get',
        url: 'http://interviewapi20170221095727.azurewebsites.net/api/Student/EnrollmentHistory?studentId={sid}'
      },
      assignment: {
        method: 'get',
        url: 'http://interviewapi20170221095727.azurewebsites.net/api/Student/AssignmentHistory?studentId={sid}'
      }
    }
  }
};


import { Component, OnInit } from '@angular/core';
import { HomeService } from './home.service';

declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  studentListArray: any;
  studentEnrollArray: any;
  studentAssArray: any;
  enrollHide: boolean;
  assignHide: boolean;
  studentId: any;

  constructor(private _homeService: HomeService) { }

  ngOnInit() {
    this.enrollHide = false;
    this.assignHide = false;
    this.studentId = 1;
    this._getStudentDetails();
  }

  _getStudentDetails() {
    this._homeService.getStudentList().subscribe(data => {
        this.studentListArray = data;
    });
  }

  onEnrollment(sid) {
    this.enrollHide = true;
    this.studentId = sid;

    this._homeService.getStudentEnrollment(sid).subscribe(data => {
        this.studentEnrollArray = data;
        $('.enroll-modal-box').modal('show');
    });
  }

  onAssignment(sid) {
    this.assignHide = true;
    this.studentId = sid;
    this._homeService.getStudentAssignment(sid).subscribe(data => {
        this.studentAssArray = data;
        $('.assign-modal-box').modal('show');
    });
  }

}

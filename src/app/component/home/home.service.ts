import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';
import { environment } from 'environments/environment';

@Injectable()

export class HomeService {

    constructor(public http: Http) {}

    /* Get Student list */
    getStudentList(): Observable<any> {
        const _headers = new Headers();
        _headers.append(environment.headers.param, environment.headers.value);
        const _options = new RequestOptions({ headers: _headers });

        return this.http[environment.api.student.view.method](environment.api.student.view.url, _options)
                    .map((res: Response) => res.json())
                    .catch(this._errorHandler);
    }

    /* Get Student Enrollment */
    getStudentEnrollment(id: any): Observable<any> {
        const _headers = new Headers();
       _headers.append(environment.headers.param, environment.headers.value);
        const _options = new RequestOptions({ headers: _headers });

        const _apiUrl = environment.api.student.enrollment.url.replace('{sid}', id);

        return this.http[environment.api.student.enrollment.method](_apiUrl, _options)
                    .map((res: Response) => res.json())
                    .catch(this._errorHandler);
    }

    /* Get Student Assignment */
    getStudentAssignment(id: any): Observable<any> {
        const _headers = new Headers();
        _headers.append(environment.headers.param, environment.headers.value);
        const _options = new RequestOptions({ headers: _headers });
        const _apiUrl = environment.api.student.assignment.url.replace('{sid}', id);

        return this.http[environment.api.student.assignment.method](_apiUrl, _options)
                    .map((res: Response) => res.json())
                    .catch(this._errorHandler);
    }

    _errorHandler(error: Response) {
        return Observable.throw(error || 'Server error');
    }
}
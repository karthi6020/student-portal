import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment';
import { LoginUserModel } from './login.model';
import { AuthService } from '../../shared/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  errorMsg: boolean;

  userModel = new LoginUserModel('', '');

  constructor(private _router: Router, private _auth: AuthService) { }

  ngOnInit() {
      this.errorMsg = false;
      if (this._auth._isLoggedIn) {
        this._router.navigate(['/home']);
      }
  }

  onSubmit() {
    const envUser = environment.userInfo.name;
    const envPwd = environment.userInfo.pwd;

    if (envUser === this.userModel.username && envPwd === this.userModel.password) {
        this._auth._authInfo(this.userModel.username);
        this._router.navigate(['/home']);
    } else {
        this.errorMsg = true;
        return false;
    }
  }

  onChangeInput() {
     this.errorMsg = false;
  }

}

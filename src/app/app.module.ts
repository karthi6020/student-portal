import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';

/* App routes */
import { appRoutes } from './app.routes';

/* App all Components */
import { AppComponent } from './app.component';
import { HomeComponent } from './component/home/home.component';
import { LoginComponent } from './component/login/login.component';

/* All component Services */
import { HomeService } from './component/home/home.service';
import { AuthService } from './shared/auth.service';
import { AuthGuardService } from './shared/auth.guard.service';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [AuthService, AuthGuardService, HomeService],
  bootstrap: [AppComponent]
})
export class AppModule { }

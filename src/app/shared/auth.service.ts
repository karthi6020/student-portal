import { Injectable } from '@angular/core';

@Injectable()
export class AuthService {

    public _userDetails: any;

    constructor() {
        this._userDetails = JSON.parse(sessionStorage.getItem('userInfo'));
    }

    _authInfo(details: any) {
        sessionStorage.setItem('userInfo', JSON.stringify(details));
        this._userDetails = JSON.parse(sessionStorage.getItem('userInfo'));
    }
    _isLoggedIn() {

        if (!this._userDetails) {
            return false;
        }
        return true;
    }
}

